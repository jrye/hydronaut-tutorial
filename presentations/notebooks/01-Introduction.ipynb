{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "94e4b1fb-6015-45bb-90c9-8315c05446ec",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "<div class=\"centered\">\n",
    "<img src=\"../img/logos/hydronaut_logo.svg\" alt=\"Hydronaut logo\" />\n",
    "<span>A framework for exploring the depths of hyperparameter space with Hydra and MLflow.</span>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ab86375e-7ecc-482b-b572-27f796365382",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    },
    "tags": []
   },
   "source": [
    "## Links\n",
    "\n",
    "<div class=\"flex_slide\"><div class=\"flex_main\">\n",
    "\n",
    "* [Tutorial homepage](https://jrye.gitlabpages.inria.fr/hydronaut-tutorial/)\n",
    "* [Tutorial Git repository](https://gitlab.inria.fr/jrye/hydronaut-tutorial/)\n",
    "* [Hydronaut Git repository](https://gitlab.inria.fr/jrye/hydronaut)\n",
    "* [Hydronaut PyPI package](https://pypi.org/project/hydronaut/)\n",
    "* [Hydronaut API documentation](https://jrye.gitlabpages.inria.fr/hydronaut/)\n",
    "    \n",
    "</div><div class=\"flex_aside\">\n",
    "<figure>\n",
    "    <img alt=\"GitLab URL\" src=\"../img/qrcode-tutorial_homepage.svg\" />\n",
    "    <figcaption>tutorial homepage</figcaption>\n",
    "</figure>\n",
    "</div></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1d4b6507-1581-4d6f-a0c0-a71bbe2ef4ba",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "# Briefly About Me\n",
    "\n",
    "**Jan-Michael (Mike) Rye**\n",
    "\n",
    "* Inria Research Engineer, SED-LYS, Lyon.\n",
    "* Member of the AIstroSight research team.\n",
    "* Creator of Hydronaut."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bccf6cc8-f4b1-4af3-8f7a-b4420b6849ce",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "# Why Was Hydronaut Developed?\n",
    "\n",
    "Hydronaut was developed in the context of a pluridisciplinary research team to address several recurring real-life problems."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d529b727-593a-43c7-913a-2c8fb2ae8826",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    },
    "tags": []
   },
   "source": [
    "## The Spaghetti Problem\n",
    "\n",
    "As an experiment increases in complexity, the number of hyperparameters tends to increase and the code to pass them around often ends up looking like spaghetti.\n",
    "\n",
    "<div class=\"flex_slide\"><div class=\"flex_main fb60\">\n",
    "\n",
    "~~~python\n",
    "def get_model(\n",
    "    model_type, loss_type='MSE', fc_layer_size=10, \n",
    "    dropout_rate_1=0.2, dropout_rate_2=0.1,\n",
    "    conv_size_1=3, conv_size_2=2, ...\n",
    "):\n",
    "    # ...\n",
    "    \n",
    "def train_model(\n",
    "    model_type, loss_type='MSE', fc_layer_size=10,\n",
    "    dropout_rate_1=0.2, dropout_rate_2=0.1,\n",
    "    conv_size_1=3, conv_size_2=2, ...\n",
    "):\n",
    "    model = get_model(\n",
    "        model_type, loss_type=loss_type,\n",
    "        fc_layer_size=fc_layer_size, ...\n",
    "    )\n",
    "    # ...\n",
    "~~~\n",
    "    \n",
    "</div><div class=\"flex_aside fb40\">\n",
    "    \n",
    "* Multiple code changes to add or modify a single parameter.\n",
    "* Function signatures rapidly become unmanageable (e.g. 20+ parameters).\n",
    "* No logical parameter hierarchy.\n",
    "* [Pylint](https://pylint.readthedocs.io/en/latest/) is very sad 😞\n",
    "    \n",
    "<figure style=\"width: 20em;\">\n",
    "    <img class=\"rounded_img\" alt=\"photo of spaghetti\" src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/Touched_by_His_Noodly_Appendage_HD.jpg/1024px-Touched_by_His_Noodly_Appendage_HD.jpg\" />\n",
    "    <figcaption><a href=\"https://commons.wikimedia.org/wiki/File:Touched_by_His_Noodly_Appendage_HD.jpg\">Wikimedia Commons: Touched by His Noodly Appendage HD.jpg</a></figcaption>\n",
    "</figure>\n",
    "</div></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "54c118a5-deda-4f7f-ade1-8ae04bfe05f9",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    },
    "tags": []
   },
   "source": [
    "## Less Spaghetti With Objects But Still Not Ideal\n",
    "    \n",
    "Object-oriented programming can alleviate some of these problems but fully addressing them requires planning and work. Researchers often don't want to deal with that.\n",
    "\n",
    "<div class=\"flex_slide\"><div class=\"flex_main\">\n",
    "\n",
    "~~~python\n",
    "class ModelTrainer():\n",
    "    def __init__(\n",
    "        self, model_type, loss_type='MSE', fc_layer_size=10,\n",
    "        dropout_rate_1=0.2, dropout_rate_2=0.1,\n",
    "        conv_size_1=3, conv_size_2=2, ...\n",
    "    ):\n",
    "        self.model_type = model_type\n",
    "        self.loss_type = loss_type\n",
    "        # ...\n",
    "        \n",
    "    def get_model(self):\n",
    "        # ...\n",
    "        \n",
    "    def train_model(self):\n",
    "        # ...\n",
    "~~~\n",
    "    \n",
    "</div><div class=\"flex_aside\">\n",
    "<figure>\n",
    "    <img class=\"rounded_img\" alt=\"photo of spaghetti\" src=\"https://upload.wikimedia.org/wikipedia/commons/d/d1/Spaghetti_with_Meatballs_%28cropped%29.jpg\" />\n",
    "    <figcaption><a href=\"https://commons.wikimedia.org/wiki/File:Spaghetti_with_Meatballs_(cropped).jpg\">Wikimedia Commons: Spaghetti with Meatballs (cropped).jpg</a></figcaption>\n",
    "</figure>\n",
    "</div></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9c0a4951-0e99-46da-b3e9-0b246c8ba6b0",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    },
    "tags": []
   },
   "source": [
    "## Nested Loops Galore For Parameter Sweeps\n",
    "\n",
    "Setting up experiments to explore the hyperparameter space all too often ends up producing something like this.\n",
    "\n",
    "<div class=\"flex_slide\"><div class=\"flex_main\">\n",
    "\n",
    "~~~sh\n",
    "for loss_type in MSE MAE HUBER; do\n",
    "    for fc_layer_size in 8 10 15 20; do\n",
    "        for dropout_rate_1 in 0.1 0.2 0.3 0.4; do\n",
    "            for dropout_rate_2 in 0.1 0.2 0.3 0.4; do\n",
    "                for ...\n",
    "                    for ...\n",
    "                        for ...\n",
    "                            # ...\n",
    "                                # Some horrendous code\n",
    "                                # here to handle all of\n",
    "                                # the hyperparameters.\n",
    "                        done\n",
    "                    done\n",
    "                done\n",
    "            done\n",
    "        done\n",
    "    done\n",
    "done\n",
    "~~~\n",
    "    \n",
    "</div><div class=\"flex_aside\">\n",
    "<figure>\n",
    "    <img class=\"rounded_img\" alt=\"Inception screenshot\" src=\"../img/01/inception.jpg\" />\n",
    "    <figcaption>Inception screenshot</figcaption>\n",
    "</figure>\n",
    "</div><div class=\"flex_aside\">\n",
    "<figure>\n",
    "    <img class=\"rounded_img\" alt=\"photo of Matryoshka dolls\" src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/8/8d/Poup%C3%A9es_russes.JPG/1365px-Poup%C3%A9es_russes.JPG\" />\n",
    "    <figcaption><a href=\"https://commons.wikimedia.org/wiki/File:Poup%C3%A9es_russes.JPG\">Wikimedia Commons: Poupées russes.JPG</a></figcaption>\n",
    "</figure>\n",
    "</div>\n",
    "</div>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "06526538-52ff-497d-8c7b-dc26e3e91a83",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    },
    "tags": []
   },
   "source": [
    "<div class=\"flex_slide\"><div class=\"flex_main fb65\">\n",
    "    \n",
    "## No Systematic Tracking Of Results\n",
    "    \n",
    "* Nobody remembers when the experiment was run or with which version of the code.\n",
    "* The exact hyperparameters and dataset used are lost to the sands of time.\n",
    "* The results and trained models are scattered across a long-gone intern's hard disk.\n",
    "* Comparing different results is needlessly complicated.\n",
    "\n",
    "</div><div class=\"flex_aside fb35\">\n",
    "<figure>\n",
    "    <img class=\"rounded_img\" alt=\"photo of a bales of paper\" src=\"https://upload.wikimedia.org/wikipedia/commons/0/01/Victory_Program._Stacking_bales_of_pressed_paper_8c34766v.jpg\" />\n",
    "    <figcaption><a href=\"https://commons.wikimedia.org/wiki/File:Victory_Program._Stacking_bales_of_pressed_paper_8c34766v.jpg\">Wikimedia Commons: Victory Program. Stacking bales of pressed paper 8c34766v.jpg</a></figcaption>\n",
    "</figure>\n",
    "</div></div>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "77c916b4-a742-477d-b427-aabc2ffad7ea",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "# How Does Hydronaut Address These Problems?\n",
    "\n",
    "Hydronaut integrates two popular libraries into a simple framework that is easy to use for non-developers:\n",
    "\n",
    "<div class=\"centered\">\n",
    "<div style=\"display: inline-block;\">\n",
    "<div style=\"display: inline-block; margin-right: 5em;\">\n",
    "<img alt=\"Hydra logo\" src=\"../img/logos/hydra.svg\" /><br /> Hydra for hyperparameter configuration\n",
    "</div>\n",
    "<div style=\"display: inline-block;\">\n",
    "<img alt=\"MLflow logo\" src=\"../img/logos/mlflow.svg\" /><br /> MLflow for experiment tracking and more\n",
    "</div>\n",
    "</div>\n",
    "</div>\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "385c44cb-3e68-4da0-9b30-91f77ae92889",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "# What is Hydra?\n",
    "\n",
    "<div>\n",
    "\n",
    "<img style=\"float: right;\" src=\"../img/logos/hydra.svg\" alt=\"Hydra logo\" />\n",
    "  \n",
    "> A framework for elegantly configuring complex applications.\n",
    "\n",
    "## Benefits\n",
    "\n",
    "* Define all hyperparameters in human-readable YAML configuration files.\n",
    "* Easy overrides from the command-line.\n",
    "* Exhaustive or optimizing exploration of hyperparameter combinations.\n",
    "* Hyperparameters are organized in a configuration object (OmegaConf).\n",
    "* Tracking of the hyperparameters used.\n",
    "\n",
    "## Links\n",
    "    \n",
    "* [Homepage](https://hydra.cc/)\n",
    "* [Documentation](https://hydra.cc/docs/intro/)\n",
    "* [PyPI package](https://pypi.org/project/hydra-core/)\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f44d8ca3-6c8b-4ac6-9f28-e64caecd5171",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": "subslide"
    },
    "tags": []
   },
   "source": [
    "## YAML Configuration Files & CLI Overrides\n",
    "\n",
    "<div class=\"flex_slide\"><div class=\"flex_main fb50\">\n",
    "\n",
    "### Example YAML Configuration File\n",
    "    \n",
    "~~~yaml\n",
    "model_type: convolutional\n",
    "loss_type: MSE\n",
    "fc_layer_size: 10\n",
    "layer_1:\n",
    "    conv_size: 3\n",
    "    dropout_rate: 0.2\n",
    "layer_2:\n",
    "    conv_size: 2\n",
    "    droput_rate: 0.1\n",
    "~~~\n",
    "\n",
    "### Command-Line Overrides\n",
    "    \n",
    "~~~sh\n",
    "run loss_type=MAE layer_1.conv_size=2\n",
    "~~~\n",
    "\n",
    "</div><div class=\"flex_aside fb50\">\n",
    "    \n",
    "* Hyperparameters are defined in one place.\n",
    "* Easy to organize hyperparameters logically.\n",
    "* Override any parameter from the command-line.\n",
    "    \n",
    "</div></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "26e0aff8-2ae4-4432-b33d-b2b07dfc92b1",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": "subslide"
    },
    "tags": []
   },
   "source": [
    "## Accessing Parameters From Code\n",
    "\n",
    "The parameters are passed around in an `Omegaconf` configuration object. Far fewer code changes are required to add, remove or otherwise modify the parameters.\n",
    "\n",
    "~~~python\n",
    "class ModelTrainer():\n",
    "    def __init__(self, config):\n",
    "        self.config = config\n",
    "        \n",
    "    def get_model(self):\n",
    "        loss_type = self.config.loss_type\n",
    "        # ...\n",
    "        dropout_1 = torch.nn.Dropout(p=self.config.layer_1.dropout_rate)\n",
    "        # ...\n",
    "        \n",
    "    def train_model(self):\n",
    "        # ...\n",
    "~~~"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b3f34983-8e96-42da-ad83-a812f15af190",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    },
    "tags": []
   },
   "source": [
    "## Systematic Parameter Sweeping\n",
    "\n",
    "Running the experiment for each combination of hyperparameters no longer requires nested loops.\n",
    "\n",
    "~~~yaml\n",
    "# Reserved section of the Hydra configuration file\n",
    "hydra:\n",
    "    sweeper:\n",
    "        params:\n",
    "            loss_type: choice(MSE, MAE)\n",
    "            fc_layer_size: choice(8, 10, 15)\n",
    "            layer_1.conv_size: range(2, 4)\n",
    "            layer_1.dropout_rate: range(0.1, 0.5, 0.1)\n",
    "            layer_2.conv_size: range(2, 4)\n",
    "            layer_1.dropout_rate: range(0.1, 0.5, 0.1)\n",
    "~~~\n",
    "\n",
    "[Syntax documentation](https://hydra.cc/docs/advanced/override_grammar/extended/)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "afab1647-86cd-46bb-929e-8ee6a8f374d6",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    },
    "tags": []
   },
   "source": [
    "## Plugins For Parameter Optimization\n",
    "\n",
    "Naïvely exploring the full parameter space is not always possible. Luckily, Hydra provides alternative sweepers via plugins (e.g. [Optuna](https://hydra.cc/docs/plugins/optuna_sweeper/)) to use different optimization algorithms.\n",
    "\n",
    "<div class=\"flex_slide\"><div class=\"flex_main fb70\">\n",
    "    \n",
    "~~~yaml\n",
    "# Another reserved section of the Hydra configuration file.\n",
    "defaults:\n",
    "    - override hydra/sweeper: optuna\n",
    "    - override hydra/sweeper/sampler: tpe\n",
    "\n",
    "hydra:\n",
    "    sweeper:\n",
    "        direction: minimize\n",
    "        n_trials: 30\n",
    "        params:\n",
    "            loss_type: choice(MSE, MAE)\n",
    "            fc_layer_size: choice(8, 10, 15)\n",
    "            layer_1.conv_size: range(2, 4)\n",
    "            layer_1.dropout_rate: interval(0.1, 0.5)\n",
    "            layer_2.conv_size: range(2, 4)\n",
    "            layer_1.dropout_rate: interval(0.1, 0.5)\n",
    "\n",
    "~~~\n",
    "    \n",
    "    \n",
    "</div><div class=\"flex_aside fb30\">\n",
    "    \n",
    "* Optimize one or more objective values.\n",
    "* Specify continuous intervals instead of discrete ranges.\n",
    "* Limit number of trials by time or number.\n",
    "* Custom options per optimizer (e.g. log-scale ranges, different optimization algorithms and stopping conditions).\n",
    "    \n",
    "</div></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "737efeae-a123-4b62-8a66-29d2b4feae6d",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    },
    "tags": []
   },
   "source": [
    "## Several Other Advantages\n",
    "\n",
    "* The configuration file can interpolate values from other fields and even user-defined function calls via so-called [resolvers](https://omegaconf.readthedocs.io/en/latest/custom_resolvers.html).\n",
    "\n",
    "    ~~~yaml\n",
    "    layer_1:\n",
    "        conv_size: 2\n",
    "        # ...\n",
    "    layer_2:\n",
    "        conv_size: ${layer_1.conv_size}\n",
    "        # ...\n",
    "    n_jobs: ${n_cpu:}\n",
    "    ~~~\n",
    "* The actual parameters used for each run are saved to disk (but we'll see even better tracking with MLflow).\n",
    "* Lots of other functionality beyond the scope of this presentation (e.g. grouping parameters in different configuration files via [packages](https://hydra.cc/docs/advanced/overriding_packages/), extensive override syntax, etc.)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3db4d224-ab17-41da-8b29-5cb4e9f0e980",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "# What is MLflow?\n",
    "\n",
    "<div>\n",
    "\n",
    "<img style=\"float: right;\" src=\"../img/logos/mlflow.svg\" alt=\"MLflow logo\" />\n",
    "  \n",
    "> An open source platform for the machine learning lifecycle.\n",
    "\n",
    "## Benefits\n",
    "\n",
    "* Systematic tracking of experiments (code, data, configuration and results for each run).\n",
    "* Web interface, command-line tool and Python API for examining, comparing and extracting results.\n",
    "* Functionality for packaging code and models for distribution and reproducibility.\n",
    "* Able to retrieve and distribute models through registry servers.\n",
    "\n",
    "## Links\n",
    "\n",
    "* [Homepage](https://mlflow.org/)\n",
    "* [Documentation](https://mlflow.org/docs/latest/index.html)\n",
    "* [PyPI package](https://pypi.org/project/mlflow/)\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3dc542f4-47dd-44cb-b53c-e05b6973b427",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    },
    "tags": []
   },
   "source": [
    "## TODO\n",
    "\n",
    "Overview of directory layout, `mlflow ui` screenshots, command-line tool, Python API (defer examples to end)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "62f94217-bb44-4e4f-8b02-8561df7c33d5",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    },
    "tags": []
   },
   "source": [
    "## Experiment Organization\n",
    "\n",
    "* Runs are grouped by experiment.\n",
    "\n",
    "![foo](../img/01/mlflow_ui/experiment_runs.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a6241bf5-0b2f-4019-9e9d-77ca3201eaba",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    },
    "tags": []
   },
   "source": [
    "## Experiment Metadata\n",
    "\n",
    "* Each run includes all associated metadata.\n",
    "* The user can configure what is associated (parameters, metrics, artifacts, etc.).\n",
    "\n",
    "![foo](../img/01/mlflow_ui/experiment_metadata.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "df6feaa9-ec24-4e10-b554-24a41d2aab11",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    },
    "tags": []
   },
   "source": [
    "## Experiment Artifacts\n",
    "\n",
    "* Each run includes user-configurable artifacts (output data, models, diagrams, etc.).\n",
    "\n",
    "![foo](../img/01/mlflow_ui/experiment_artifacts.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1e9c63af-9cd2-43c9-b12d-fcb2649e271e",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    },
    "tags": []
   },
   "source": [
    "## Experiment Comparison\n",
    "\n",
    "* Easy to compare different experiments by common metrics.\n",
    "\n",
    "![foo](../img/01/mlflow_ui/all_runs.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "758e6375-eb9d-45b7-94e5-c33122ba6d15",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    },
    "tags": []
   },
   "source": [
    "## Experiment Comparison Continued\n",
    "\n",
    "* Basic visualization is available in the web interface to get a quick overview of the results.\n",
    "\n",
    "![foo](../img/01/mlflow_ui/simple_comparison.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "293c5b41-44e3-4a67-a422-ed93bb58d814",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    },
    "tags": []
   },
   "source": [
    "## Much More\n",
    "\n",
    "* Automation via `mlflow` command-line interface:\n",
    "    \n",
    "    <div class=\"ch16\">\n",
    "    \n",
    "    ~~~\n",
    "    $ mlflow --help\n",
    "    Usage: mlflow [OPTIONS] COMMAND [ARGS]...\n",
    "\n",
    "    Options:\n",
    "      --version  Show the version and exit.\n",
    "      --help     Show this message and exit.\n",
    "\n",
    "    Commands:\n",
    "      artifacts    Upload, list, and download artifacts from an MLflow...\n",
    "      db           Commands for managing an MLflow tracking database.\n",
    "      deployments  Deploy MLflow models to custom targets.\n",
    "      doctor       Prints out useful information for debugging issues with MLflow.\n",
    "      experiments  Manage experiments.\n",
    "      gc           Permanently delete runs in the `deleted` lifecycle stage.\n",
    "      models       Deploy MLflow models locally.\n",
    "      recipes      Run MLflow Recipes and inspect recipe results.\n",
    "      run          Run an MLflow project from the given URI.\n",
    "      runs         Manage runs.\n",
    "      sagemaker    Serve models on SageMaker.\n",
    "      server       Run the MLflow tracking server.\n",
    "    ~~~\n",
    "    \n",
    "    </div>\n",
    "    \n",
    "* Automation via [MLflow Python API](https://www.mlflow.org/docs/latest/python_api/index.html)\n",
    "* Automatic artifact logging for PyTorch Lightning, TensorFlow, sklearn, etc. via the Python API.\n",
    "* Tracking servers, central registries, etc."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6ea40c4a-fd1b-440a-ad27-db69fb6223a3",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "# What Does Hydronaut Bring To The Table?\n",
    "\n",
    "Hydronaut spares the user from having to write all of the boiler-plate code and handle various quirks when using Hydra and MLflow together."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8bf1819d-ee40-464b-82f6-a2cf37c00e9d",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    },
    "tags": []
   },
   "source": [
    "## Relative Simplicity\n",
    "\n",
    "Hydronaut provides the following:\n",
    "\n",
    "* A simple framework that only requires the user to:\n",
    "    - Create a YAML configuration file.\n",
    "    - Create a custom subclass with a single method that returns one or more objective values.\n",
    "    - Use the configuration object to retrieve hyperparameters.\n",
    "* A command named `hydronaut-init` to facilitate initial setup.\n",
    "* Custom resolvers for Hydra/OmegaConf (# cpus, # gpus, etc.).\n",
    "* Convenience functions for working with Hydra/OmegaConf configuration objects.\n",
    "* Subclasses to simplify common use cases (only PyTorch Lighting at the moment but more to come)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1a802441-0e97-45cb-a958-79554a5d415c",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    },
    "tags": []
   },
   "source": [
    "## Handles The Boring Stuff\n",
    "\n",
    "* Automates some MLflow logging:\n",
    "    - All experiment parameters in the configuration file are logged as parameters.\n",
    "    - The resulting per-run configuration file itself is logged as an artifact.\n",
    "    - Logs the objective values as metrics.\n",
    "* Wraps the [MLflow logging functions](https://mlflow.org/docs/latest/python_api/mlflow.html#mlflow.log_artifact) for simpler access.\n",
    "* Configures Python paths at runtime for importing user code without installation.\n",
    "* Sets up MLflow with values from the configuration file.\n",
    "* Configures the runtime environment to ensure that MLflow runs are correctly tracked (e.g. in multiprocess runs)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fb1a2199-0295-4cca-a2ee-00c9da1e9d51",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    },
    "tags": []
   },
   "source": [
    "## Provides Some Structure\n",
    "\n",
    "* Recommends a basic directory hierarchy for each experiment:\n",
    "\n",
    "    ~~~\n",
    "    my_experiment\n",
    "    ├── conf\n",
    "    │   └── config.yaml\n",
    "    └── src\n",
    "        └── experiment.py\n",
    "    ~~~\n",
    "\n",
    "* Requires some custom fields in the configuration file:\n",
    "\n",
    "    ~~~yaml\n",
    "    experiment:\n",
    "        name: My Amazing Experiment\n",
    "        description: The most amazing experiment in the world.\n",
    "        params:\n",
    "            awesomeness: 10\n",
    "            # ...\n",
    "    ~~~\n",
    "* Optionally imposes some structure on the configuration file via data classes."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a6528688-ff48-424c-ad7d-866a8dbc3a5e",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "# So How Do We Use It?\n",
    "\n",
    "* The Hydronaut Git repository provides [examples](https://gitlab.inria.fr/jrye/hydronaut/-/tree/main/examples).\n",
    "* Jupyter notebook tutorials are provided in the same [Git repository as this presentation](https://gitlab.inria.fr/jrye/hydronaut-tutorial).\n",
    "* The notebooks are exported to slides and published online [here](https://jrye.gitlabpages.inria.fr/hydronaut-tutorial/)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "29b2e60c-eab8-4cd0-b54d-c95d11d7a24a",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "# The End\n",
    "\n",
    "<div class=\"flex_slide\"><div class=\"flex_main\">\n",
    "    \n",
    "* [Back to homepage](../index.html)\n",
    "\n",
    "</div><div class=\"flex_aside\">\n",
    "<figure>\n",
    "    <img alt=\"QR code with homepage URL \" src=\"../img/qrcode-tutorial_homepage.svg\" />\n",
    "    <figcaption>tutorial homepage</figcaption>\n",
    "</figure>\n",
    "</div></div>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.6"
  },
  "toc-autonumbering": false,
  "toc-showcode": false,
  "toc-showmarkdowntxt": false
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
