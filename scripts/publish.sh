#!/usr/bin/env bash
set -euo pipefail

SELF=$(readlink -f "${BASH_SOURCE[0]}")
cd -- "${SELF%/*/*}"

./scripts/run_jnp_in_venv.sh -c ./jnp.yaml
