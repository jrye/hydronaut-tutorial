#!/usr/bin/env bash
set -euo pipefail

SELF=$(readlink -f "${BASH_SOURCE[0]}")
cd -- "${SELF%/*/*}"

if [[ ! -e venv ]]
then
  python3 -m venv venv
  source venv/bin/activate
  pip install -U pip
else
  source venv/bin/activate
fi

if ! python -c 'import jnp' > /dev/null 2>&1
then
  if [[ ! -e submodules/jnp/README.md ]]
  then
    git submodule update --init --recursive
  fi
  pip install -U submodules/jnp
fi

jnp "$@"
