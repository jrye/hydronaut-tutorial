#!/usr/bin/env bash
set -euo pipefail

SELF=$(readlink -f "${BASH_SOURCE[0]}")
cd -- "${SELF%/*/*}"

if [[ ! -e venv ]]
then
  python3 -m venv venv
  source venv/bin/activate
else
  source venv/bin/activate
fi

pip install -U ipykernel hydronaut

kernel_name=hydronaut_venv
py_exe=$(jupyter kernelspec list --json | jq  -r ".kernelspecs.$kernel_name.spec.argv[0]")
expected_py_exe=$(command -v python)
if [[ $py_exe != "$expected_py_exe" ]]
then
  if [[ $py_exe != "null" ]]
  then
    jupyter kernelspec uninstall -f "$kernel_name"
  fi
  ipython kernel install --user --name="$kernel_name"
fi
jupyter-lab
